1. Instala todas las dependencias npm.
2. Crea un servidor con Express, con las siguientes características:
    - Usará el motor de plantillas Handlebars.
    - Las vistas de Handlebars estarán en web/views, configúralo.
    - Los recursos estáticos estarán en web/public, configúralo.
    - Cuando se lance una petición GET a /, se debe renderizar la vista index.hbs.
    - La vista se debe renderizar con los datos obtenidos de una API de facturas.
3. Clónate este repositorio para montar una API REST de facturas: https://bitbucket.org/nodejs-madrid/facturas-api.git . Hazlo en una carpeta fuera de este proyecto.
4. Instala las dependencias de la API REST de facturas y lánzala con npm start.
5. Vuelve a tu servidor www, y haz que al recibir la petición GET a /, se conecte a la API de facturas para pedir el listado de facturas.
6. Envíale las facturas a la vista de Handlebars.
7. Comprueba que la web se está renderizando correctamente.